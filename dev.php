<?php
$dev_mode = true;
require('index.php');
return;
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Development Tools; Open Celtic Dictionary</title>
		<meta charset="utf-8"/>
		<script type="text/javascript" src="script.js"></script>
		<script type="text/javascript" src="scripts/word.js"></script>
		<script type="text/javascript" src="scripts/idlist.js"></script>
		<script type="text/javascript" src="scripts/generatejson.js"></script>
		<script type="text/javascript" src="scripts/generatetense.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="icon" type="image/png" href="digi_v2.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<h1>Development Tools for the Open Celtic Dictionary</h1>
	</body>
</html>
