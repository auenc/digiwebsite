<?php

$page = "dictionary";
if(isset($_GET['page'])){
	if(preg_match("/[a-z]+/", $_GET['page']))
		$page = $_GET['page'];
}


$translations = json_decode(file_get_contents("translations.json"), true);

$lang = "en";

// See if get variable 'lang is set
if(isset($_GET['lang'])){
	$lang = $_GET['lang'];
	//setcookie('lang', $lang, time() + (84000*7), '/');
}


// Restrict languages to only support these
$languages = array("en", "br", "cy", "ga", "gd");
if(!in_array($lang, $languages))
	$lang = "en";


/*
 * Fetches the text with the given string in translations.
 * If the translations contains a translation for the current language return that,
 * otherwise return the English version.
 */
function s($key){
	global $translations, $lang;
	if(!isset($translations[$key]))
		throw new Exception('Missing key: ' + $key);
	if(isset($translations[$key][$lang]))
		return $translations[$key][$lang];
	return $translations[$key]["en"];
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Open Celtic Dictionary; Digi Web-Interface</title>
		<meta charset="utf-8"/>
		<meta name="author" content="The Open Celtic Dictionary Project">
		<meta name=”description” content="The Open Celtic Dictionary Project. Free and open dictionary for Welsh, Breton, Irish, and Scottish Gaelic.">
		<meta name="keywords" content="Dictionary, Welsh, Breton, Irish, Gaelic, Scottish Gaelic, Open">
		<script type="text/javascript" src="translations.js"></script>
		<script type="text/javascript" src="script.js"></script>
		<script type="text/javascript" src="scripts/word.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="icon" type="image/png" href="digi_v2.png" />
		<link rel="manifest" href="manifest.json">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script>
			const lang = "<?=$lang ?>";
		</script>
	</head>
	<body>
		<h1><?=s('header')?></h1>
		<nav>
			<button class="tabButton" onclick="setPage({'page': 'dictionary'})"><?=s('dictionary')?></button>
			<button class="tabButton" onclick="setPage({'page': 'number'})"><?=s('numbers')?></button>
			<button class="tabButton" onclick="setPage({'page': 'translation'})"><?=s('translations')?></button>
			<button class="tabButton" onclick="setPage({'page': 'help'})"><?=s('help')?></button>
			<button class="tabButton" onclick="setPage({'page': 'about'})"><?=s('about')?></button>
			<button class="tabButton" onclick="setPage({'page': 'settings'})"><?=s('settings')?></button>
		</nav>
<?php if($dev_mode): ?>
		<nav>
			<button class="tabButton" onclick="setPage({'page': 'testjson'})">JSON Tester</button>
			<button class="tabButton" onclick="setPage({'page': 'generatejson'})">JSON Generator</button>
			<button class="tabButton" onclick="setPage({'page': 'generatetense'})">Tense Generator</button>
			<button class="tabButton" onclick="setPage({'page': 'idlist'})">ID List</button>
		</nav>
<?php endif; ?>
		<?php
			$php_path = 'pages/' . $page . '.php';
			$js_path = 'scripts/' . $page . '.js';

			if(file_exists($js_path))
				echo "<script type='text/javascript' src='{$js_path}'></script>\n";

			if(file_exists($php_path))
				require($php_path);
			else{
				require('pages/404.php');
			}
		?>
	</body>
</html>

