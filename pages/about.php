<div id="about" class="results">
	<div class="result">
		<h2>The Open Celtic Dictionary Project</h2>
		<h3>About the project</h3>
		<p>
		The project was stared during the spring of 2020 in order to try to make an open-source dictonary API for all of the 6 currently living Celtic Languages.
		We are always looking for new volunteers, so if you want to help add new words, report any bugs, improve the website, or have other ideas please contact us on Discord or Gitlab (links bellow) :)
		</p>
		<p>
		The project also have it's own Discord bot which you are able to invite to your server.
		His name is Digi and can be found <a href="https://gitlab.com/prvInSpace/digi">here.</a>
		He can be invited to any Discord server using the following link: 
		<a href="https://discord.com/api/oauth2/authorize?client_id=695552413066854502&permissions=346176&scope=bot">Click me!</a>
		</p>
		<h3>Community</h3>
		<p>
		The community for the project can be found over at Discord.
		You are more than welcome so feel free to join: 
		<a href="https://discord.gg/UzaFmfV">Click me!</a>
		</p>
		<h3>Special Thanks</h3>
		<p>
		I want to thank everyone who as contributed in anyway to this project. Without your support this would never have been possible. So from the bottom of my heart: Thank you!
		<br /><br/>
		- Preben 'prv' Vangberg.
		<p>
		<h3>External Resources</h3>				
		<table>
			<tr>
				<th>Discord</th>
				<td><a href="https://discord.gg/UzaFmfV">Invite Link</a></td>
			</tr>
			<tr>
				<th>Digi, the Discord bot</th>
				<td><a href="https://discord.com/api/oauth2/authorize?client_id=695552413066854502&permissions=346176&scope=bot">Invite link</a></td>
			</tr>
			<tr>
				<th>Twitter</th>
				<td><a href="https://twitter.com/OpenCelticDict">Link to Twitter</a></td>
			</tr>
		</table>
		<h3>Repositories</h3>
		<table>
			<tr>
				<th>API Server</th>
				<td><a href="https://gitlab.com/prvInSpace/open-celtic-dictionary-api-server">Link to repo</a></td>
			</tr>
			<tr>
				<th>Breton Dictionary</th>
				<td><a href="https://gitlab.com/prvInSpace/open-breton-dictionary">Link to repo</a></td>
			</tr>
			<tr>
				<th>Cornish Dictionary</th>
				<td><a href="https://gitlab.com/prvInSpace/open-cornish-dictionary">Link to repo</a></td>
			</tr>
			<tr>
				<th>Irish Dictionary</th>
				<td><a href="https://gitlab.com/prvInSpace/open-irish-dictionary">Link to repo</a></td>
			</tr>
			<tr>
				<th>Scottish Gaelic Dictionary</th>
				<td><a href="https://gitlab.com/prvInSpace/open-scottish-gaelic-dictionary">Link to repo</a></td>
			</tr>
			<tr>
				<th>Welsh Dictionary</th>
				<td><a href="https://gitlab.com/prvInSpace/open-welsh-dictionary">Link to repo</a></td>
			</tr>
			<tr>
				<th>Digi, Discord Bot</th>
				<td><a href="https://gitlab.com/prvInSpace/digi">Link to repo</a></td>
			</tr>
		</table>
		<h3>Donations</h3>
		<p>
			Because I have been asked about ways to donate to the project in the past
			 I have decided to make this section in case you are considering donating to the project.
		</p>
		<p>
			I want to make it clear that I am working on this project due to my love for minority languages,
			because I want to help others, and because it is a fun project to work on.
			Therefore if you want to donate any money I would kindly ask you to consider donating to a charity instead.
			You can decide whatever charity you want.
			If you don't know which one to choose I will try to maintain a short list here of some charities
			to pick from.
		</p>
		<table>
			<tr>
				<th>Save the Children</th>
				<td><a href="https://www.savethechildren.org/">Link</a></td>
			</tr>
			<tr>
				<th>Mind</th>
				<td><a href="https://www.mind.org.uk/">Link</a></td>
			</tr>
			<tr>
				<th>Samaritans</th>
				<td>
					<a href="https://samaritanshope.org/">Link (International)</a>
					<a href="https://www.samaritans.org/wales">Link (Wales)</a>
				</td>
			</tr>
			<tr>
				<th>Alzheimer's Society</th>
				<td><a href="https://www.alzheimers.org.uk">Link</a></td>
			</tr>
		</table>
		<p>
			I would of course be incredibly delighted and grateful if you decide to donate to a charity in the name of the project.
			Therefore if you do decide to do that, then please let me know on Twitter or on Discord 😊
		</p>
		<p>
			If I have still not convinced you to donate to a charity instead I have a BuyMeACoffee account which you can use to
			send me a cup of coffee. It can be found
			<a href="https://www.buymeacoffee.com/prvInSpace">here</a>.
		</p>
	</div>
</div>
