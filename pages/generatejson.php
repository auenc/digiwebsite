<div id="generateJson" class="tab">
	<div>
		<select id="lang">
			<option value="br">Breton</option>
			<option value="kw">Cornish</option>
			<option value="de">German</option>
			<option value="ga">Irish</option>
			<option value="gd">Scottish Gaelic</option>
			<option value="cy">Welsh</option>
		</select>
		<input type="number" placeholder="Start index", id="startIndex">
	</div>
	<div>
		<input type="text" id="newWord" placeholder="Add word here" onkeypress="return keyPress(event, addWordToList)">
		<input type="button" value="Generate" onclick="generateJson()">
		<input type="button" value="Clear words" onclick="clearWordList()">
	</div>
</div>
<div id="results" class="tab results"></div>
