
<div id="settings" class="tab results">
	<div class="result">
		<h2>Settings</h2>
		<!--<p><strong>
	  NB: By changing any of these settings you are concenting to the use of cookies in order for the website to remember these settings.
	  For more information read our <button class="clickableWord" onclick="openTab(['cookies'])" style="font-weight: bold;">cookie policy.</button>
	  </strong><p>-->
	  <h3>Language</h3>
	  <p>
	  You are able to change the interface language here. Translations might not be complete.
	  </p>
	  <table>
		  <tr>
			  <td>Language</td>
			  <td>
				  <select onchange=reloadPage('lang='+this.options[this.selectedIndex].value)>
					  <option value="en" <?php if($lang=="en")echo "selected";?>>English</option>
					  <option value="br" <?php if($lang=="br")echo "selected";?>>Breton | Brezhoneg</option>
					  <!--<option value="ga" <?php if($lang=="ga")echo "selected";?>>Irish | Gaeilge</option>-->
					  <!--<option value="gd" <?php if($lang=="gd")echo "selected";?>>Scottish Gaelic | Gàidhlig</option>-->
					  <option value="cy" <?php if($lang=="cy")echo "selected";?>>Welsh | Cymraeg</option>
				  </select>
			  </td>
		  </tr>
	  </table>
	  <!--
	   <h3>Cookies</h3>
	   <input type="button" value="Delete all cookies and remove all settings" onclick="deleteCookies()">
	  -->
	</div>
</div>
