
"use strict";

function generateList(){
	var min = document.getElementById("min").value;
	if(!min)
		min = 0;
	var max = document.getElementById("max").value;
	if(!max)
		max = 1000 + Number(min);
	var lang = document.getElementById("lang").value;

	var url = api + "/api/dictionary/idlist" + 
		"?min=" + min +
		"&max=" + max +
		"&lang=" + lang;

	fetchAndHandleJsonRequest(url, idlistToDiv);
}


function idlistToDiv(results, obj){
	var lang = document.getElementById("lang").value;
	
	var div = document.createElement('div');
	div.classList.add('result')
	div.append(createTextElement('h2', 'ID List'));
	div.append(createTextElement('h3',
		'Between ' + obj.min + ' and ' + obj.max));

	var table = document.createElement('table');
	var head = document.createElement('tr');
	head.append(createTextElement('th', 'ID'));
	head.append(createTextElement('th', 'Word'));
	for(var i = obj.min; i <= obj.max; ++i){
		var row = document.createElement('tr');
		row.append(createTextElement('td', i));
		var index = '' + i;
		if(index in obj.results){
			var td = document.createElement('td');
			var word = {};
			word['lang'] = lang;
			word['id'] = i;
			word['value'] = obj.results[index];
			td.append(createClickableWord(word));
			row.append(td);
		}
		else
			row.append(createTextElement('td', ''))
		table.append(row);
	}

	div.append(table);
	results.append(div);
}



