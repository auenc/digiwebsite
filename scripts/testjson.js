
function generateWord(){
	var input = document.getElementById("json").value;
	var lang = document.getElementById("lang").value;
	var type = document.getElementById("type").value;

	var url = api + "/api/dictionary/generate" + 
		"?json=" + escape(input) +
		"&type=" + type +
		"&lang=" + lang;

	fetchAndHandleJsonRequest(url, wordListToDiv);
}

