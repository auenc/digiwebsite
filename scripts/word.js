function wordListToDiv(results, obj){
	if(obj.results.length > 0){
		obj.results.forEach(function(word){
			results.appendChild(convertWordToDiv(word))
		});
	}
	else {
		var div = document.createElement("div")
		div.classList.add('result');
		div.appendChild(createTextElement('h2', 'No results found'));
		div.appendChild(createTextElement('h3', ':('));
		results.appendChild(div);
	}
}

function convertWordToDiv(word) {
	var div = document.createElement("div")
	div.classList.add('result');
	var normalForm = createTextElement("h2", capitalize(word.normalForm));
	var mutations = createTextElement("h3", s("mutations"));
	var conjugations = createTextElement("h3", s("conjugations"));
	var inflections = createTextElement("h3", s("inflections"))

	div.appendChild(normalForm);
	div.append(createTextElement('h3', s('basic_information')));
	var base = createBaseInformationTable(word);
	div.appendChild(base);
	var translations = createTranslationTable(word.translations, [word.lang]);
	if(translations){
		div.append(createTextElement('h3', s('translations')));
		div.append(translations);
	}

	if('examples' in word){
		div.appendChild(createTextElement('h3',s('usage_examples')));
		var examples = document.createElement('table');
		word.examples.forEach(function(e){
			examples.appendChild(createUsageExample(e));
		});
		div.appendChild(examples);
	}

	if ("mutations" in word)
		if (word.mutations.soft !== "" || word.mutations.nasal !== "" || word.mutations.aspirate !== ""){
			div.appendChild(mutations);
			div.appendChild(getMutationTable(word));

	}
	if (word.type == "adjective" && 'inflections' in word)
		div.appendChild(printAdjective(word.inflections));

	if(word.type == "adposition" && 'inflections' in word){
		div.appendChild(inflections);
		div.appendChild(createWordTenseTable(s("inflections"), word.inflections));
	}

	if(word.type == "verb" && 'conjugations' in word){
		div.appendChild(conjugations);

		tenses.forEach(function(key){
			if(key in word.conjugations){
				if(typeof word.conjugations[key] === 'string' || word.conjugations[key] instanceof String)
					base.append(createRow(s('tense_' + key), word.conjugations[key]));
				else
					div.append(createWordTenseTable(s('tense_' + key), word.conjugations[key]));
			}
		});
	}
	return div;
}

function createUsageExample(example){
	var row = document.createElement('tr');
	var div = document.createElement('td');
	var p = createTextElement('p', example.text);
	if('trans' in example){
		p.appendChild(document.createElement('br'));
		p.appendChild(createTextElement('i', example.trans));
	}
	if('explanation' in example){
		p.appendChild(document.createElement('br'));
		p.appendChild(createTextElement('p', example.explanation));
	}
	div.append(p);
	row.appendChild(div);
	return row;
}


function createBaseInformationTable(word){
	var table = document.createElement("table");
	table.appendChild(createRow(s('language'), s(languageCodeToName[word.lang])));
	table.appendChild(createRow(s('type'), s(word.type)));
	table.appendChild(createRow(s('status'), word.confirmed ? s('confirmed') : s("generated")));
	if('inflections' in word && 'plural' in word.inflections)
		table.appendChild(createRow(s('plural'), word.inflections.plural.join('<br />')));
	if('gender' in word)
		table.appendChild(createRow(s('gender'), s(genders[word.gender])));
	if('etymology' in word)
		table.appendChild(createRow(s('etymology'), word.etymology));
	if('versions' in word && word.versions.length > 0)
		table.appendChild(createRow(s('versions'), createListOfIdValueWords(word.versions)));
	if('notes' in word)
		table.appendChild(createRow(s('notes'), createTextElement('p', word.notes)));
	if('related' in word && word.related.length > 0)
		table.appendChild(createRow(s('related'), createListOfIdValueWords(word.related)));
	if('synonyms' in word && word.synonyms.length > 0)
		table.appendChild(createRow(s('synonyms'), createListOfIdValueWords(word.synonyms)));
	if('antonyms' in word && word.antonyms.length > 0)
		table.appendChild(createRow(s('antonyms'), createListOfIdValueWords(word.antonyms)));
	if('troponyms' in word && word.troponyms.length > 0)
		table.appendChild(createRow(s('troponyms'), createListOfIdValueWords(word.troponyms)));
	if('verbalNoun' in word)
		table.appendChild(createRow(s('verbal_noun'), word.verbalNoun.join('<br />')));
	if('past_participle' in word)
		table.appendChild(createRow(s('past_participle'), word.past_participle.join('<br />')));

	var links = [];
	for(var title in externalSites){
		if(externalSites[title].show(word)){
			var link = externalSites[title].link(word);
			var elm = document.createElement('a');
			elm.setAttribute('href', link);
			elm.innerHTML = title;
			links.push(elm);
		}
	}
	if(links.length > 0)
		table.appendChild(createRow(s('external_sites'), links));


	return table;
}

function createListOfIdValueWords(words){
	var p = document.createElement('div');
	words.forEach(function(word){
		if('id' in word && word.id >= 0)
			p.appendChild(createClickableWord(word));
		else
			p.appendChild(createTextElement('span', word.value));
		p.appendChild(document.createElement('br'));
	});
	return p;
}

function createTranslationTable(translationUnits, exlude = [] ){
	var translations = {};
	var found = {};
	translationUnits.forEach(function(unit){
		for(var language in unit){
			if(exlude.includes(language))
				continue;
			if(!(language in translations)){
				translations[language] = [];
				found[language] = [];
			}
			unit[language].forEach(function(translation){
				if(found[language].indexOf(translation.value) < 0){
					found[language].push(translation.value);
					if('id' in translation && translation.id >= 0){
						translations[language].push(createClickableWord(translation));
					}
					else
						translations[language].push(createTextElement('span', translation.value));
				}
			});
		}
	});
	if(Object.keys(translations).length == 0)
		return null;

	var translationKeys = Object.keys(translations).sort(function(a, b){
		var lang1 = s(languageCodeToName[a]);
		var lang2 = s(languageCodeToName[b])
		if(lang1 < lang2)
			return -1;
		if(lang2 < lang1)
			return 1;
		return 0;
	});

	var table = document.createElement('table');
	translationKeys.forEach(function(lang){
		var tr = document.createElement('tr');
		tr.appendChild(createTextElement('th', s(languageCodeToName[lang])));
		var td = document.createElement('td');
		for(var i = translations[lang].length -1; i >= 0; --i){
			td.appendChild(translations[lang][i]);
			if(i != 0)
				td.appendChild(document.createElement('br'))
		}
		tr.appendChild(td);
		table.appendChild(tr);
	});
	return table;
}

function getMutationTable(word){
	var table = document.createElement("table");
	var head = document.createElement("tr");
	var row = document.createElement("tr");

	var mutationTable = mutationsTable[word.lang];
	mutationTable.forEach(function(mutation){
		head.appendChild(createTextElement('th', s('mutation_' + mutation)));
		row.appendChild(createTextElement('td', word.mutations[mutation]));
	});

	table.appendChild(head);
	table.appendChild(row);
	return table;
}

function createWordTenseTable(header, tense){
	var table = document.createElement("table");
	table.classList.add('tense');
	var head = document.createElement("tr");
	head.appendChild(createTextElement("th", header));
	head.appendChild(createTextElement("th", s("singular")));
	head.appendChild(createTextElement("th", s("plural")));
	table.appendChild(head)	
	// Eg og vi

	if (("singFirst" in tense && tense.singFirst.length > 0) || ("plurFirst" in tense && tense.plurFirst.length > 0)){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("first")));
		row1.appendChild(createTextElement("td", "singFirst" in tense ? tense.singFirst.join("<br />") : ""));
		row1.appendChild(createTextElement("td", "plurFirst" in tense ? tense.plurFirst.join("<br />") : ""));
		table.appendChild(row1);
	}
	// Du og dere 
	if (("singSecond" in tense && tense.singSecond.length > 0) || ("plurSecond" in tense && tense.plurSecond.length > 0)){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("second")));
		row1.appendChild(createTextElement("td", "singSecond" in tense ? tense.singSecond.join("<br />") : ""));
		row1.appendChild(createTextElement("td", "plurSecond" in tense ? tense.plurSecond.join("<br />") : ""));
		table.appendChild(row1);
	}
	// Han ho, de, dei
	if (("singThird" in tense && tense.singThird.length > 0) || ("plurThird" in tense && tense.plurThird.length > 0)){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("third")));
		row1.appendChild(createTextElement("td", "singThird" in tense ? tense.singThird.join("<br />") : ""));
		row1.appendChild(createTextElement("td", "plurThird" in tense ? tense.plurThird.join("<br />") : ""));
		table.appendChild(row1);
	}
	// Han
	if ("singThirdMasc" in tense && tense.singThirdMasc.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("third_masculine")));
		row1.appendChild(createTextElement("td", tense.singThirdMasc.join("<br />")));
		row1.appendChild(createTextElement("td", ""));
		table.appendChild(row1);
	}
	//ho
	if ("singThirdFem" in tense && tense.singThirdFem.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("third_feminine")));
		row1.appendChild(createTextElement("td", tense.singThirdFem.join("<br />")));
		row1.appendChild(createTextElement("td", ""));
		table.appendChild(row1);
	}
	// Nåke
	if ("impersonal" in tense && tense.impersonal.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("impersonal")));
		var td = createTextElement("td", tense.impersonal.join("<br />"));
		td.colSpan = 2;
		row1.appendChild(td);
		table.appendChild(row1);
	}
	// Voodoo shit
	if ("analytic" in tense && tense.analytic.length > 0){
		var row1 = document.createElement("tr");
		row1.appendChild(createTextElement("th", s("analytic")));
		var td = createTextElement("td", tense.analytic.join("<br />"));
		td.colSpan = 2;
		row1.appendChild(td);
		table.appendChild(row1);
	}

	return table;
}

function printAdjective(word){
	var table = document.createElement("table");
	var head = document.createElement("tr");
	head.appendChild(createTextElement("th", s("equative")));
	head.appendChild(createTextElement("th", s("comparative")));
	head.appendChild(createTextElement("th", s("superlative")));
	if('exclamative' in word)
		head.appendChild(createTextElement('th', s("exclamative")));
	table.appendChild(head);
	var row1 = document.createElement("tr");
	row1.appendChild(createTextElement("td", word.equative.join("<br />")));
	row1.appendChild(createTextElement("td", word.comparative.join("<br />")));
	row1.appendChild(createTextElement("td", word.superlative.join("<br />")));
	if('exclamative' in word)
		row1.appendChild(createTextElement('td', word.exclamative.join("<br />")));
	table.appendChild(row1);
	return table;
}

const externalSites = {
	"Wiktionary": {
		"link": (w) => "https://en.wiktionary.org/wiki/"
			+ w.normalForm
			+ "#" + wiktionaryHeaders[w.lang],
		"show": (w) => true
	},
	// Breton
	"Displeger Berboù Brezhonek": {
		"link": (w) => "https://displeger.bzh/br/verb/" + w.normalForm,
		"show": (w) => w['lang'] == "br" && w['type'] == 'verb'
	},
	"Geirafurch": {
		"link": (w) => "https://geriafurch.bzh/br?d=brfr&q=" + w.normalForm,
		"show": (w) => w['lang'] == "br"
	},
	// Cornish
	"Gerlyver Kernewek": {
		"link": (w) => "https://www.cornishdictionary.org.uk/?locale=en#" + w.normalForm,
		"show": (w) => w['lang'] == "kw"
	},
	// Irish
	"Teanglann": {
		"link": (w) => "https://www.teanglann.ie/ga/fgb/" + w.normalForm,
		"show": (w) => w['lang'] == "ga"
	},
	// Welsh
	"Geiriadur Bangor": {
		"link": (w) => "http://geiriadur.bangor.ac.uk/#" + w.normalForm,
		"show": (w) => w['lang'] == "cy"
	},
	// German
	"Digitales Wörterbuch der deutschen Sprache": {
		"link": (w) => "https://www.dwds.de/wb/" + w.normalForm,
		"show": (w) => w['lang'] == "de"
	}
}

const mutationsTable = {
	"cy": [
		"init",
		"soft",
		"nasal",
		"aspirate"
	],
	"br": [
		"init",
		"soft",
		"spirant",
		"hard",
		"mixed"
	],
	"kw": [
		"init",
		"soft",
		"aspirate",
		"hard",
		"mixed"
	]
}

const wiktionaryHeaders = {
	"cy": "Welsh",
	"br": "Breton",
	"ga": "Irish",
	"gd": "Scottish_Gaelic",
	"gv": "Manx",
	"kw": "Cornish",
	"es": "Spanish",
	"de": "German"
}

const languageCodeToName = {
	"en": "english",
	"cy": "welsh",
	"br": "breton",
	"de": "german",
	"es": "spanish",
	"fr": "french",
	"ga": "irish",
	"gd": "scottish_gaelic",
	"gv": "manx",
	"kw": "cornish",
	"no": "norwegian",
	"nn": "norwegian"
}

const genders = {
	"m": "masculine",
	"f": "feminine",
	"mf": "masculine_feminine",
	"u": "unknown"
}

const tenses = [
	"present", 
	"present_short", 
	"present_long", 
	"present_future",
	"present_independent", 
	"present_dependent", 
	"present_affirmative",
	"present_interrogative",
	"present_negative", 
	"present_situative",
	"present_habitual",
	"past", 
	"preterite", 
	"past_independent", 
	"past_dependent", 
	"past_habitual", 
	"imperfect", 
	"imperfect_short", 
	"imperfect_long", 
	"imperfect_situative",
	"imperfect_habitual",
	"imperfect_affirmative",
	"imperfect_negative",
	"imperfect_interrogative",
	"future", 
	"future_habitual",
	"future_independent", 
	"future_dependent", 
	"future_relative", 
	"conditional", 
	"conditional_present", 
	"conditional_imperfect", 
	"conditional_independent", 
	"conditional_dependent", 
	"subjunctive_present_future",
	"subjunctive_imperfect",
	"imperative"
]

